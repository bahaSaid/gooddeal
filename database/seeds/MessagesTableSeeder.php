<?php

use Illuminate\Database\Seeder;
use App\Message;


class MessagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0;');
         // Let's truncate our existing records to start from scratch.
         Message::truncate();

         $faker = \Faker\Factory::create();
 
         // And now, let's create a few articles in our database:
         for ($i = 1; $i < 6; $i++) {
            Message::create([
                'message' => $faker->paragraph($nbSentences = 2, $variableNbSentences = true) ,
                'buyer_id' => $i,
                'seller_id' => $i+6,
                'sender_id' => $i,
                'ad_id' => $faker->numberBetween(0,20),
             
             ]);
         }

         for ($i = 1; $i < 6; $i++) {
            Message::create([
                'message' => $faker->paragraph($nbSentences = 2, $variableNbSentences = true) ,
                'buyer_id' => $i+6,
                'seller_id' => $i,
                'sender_id' => $i+6,
                'ad_id' => $faker->numberBetween(0,25),
             
             ]);
         }

         DB::statement('SET FOREIGN_KEY_CHECKS=1;');
    }
}
